package cn.chench.oauth2;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.ServerAuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import cn.hutool.json.JSONUtil;
import reactor.core.publisher.Mono;

/**
 * 自定义返回结果：没有登录或token过期时
 */
@Component
public class OauthAuthenticationEntryPoint implements ServerAuthenticationEntryPoint {
    public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException e) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.OK);
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        Map<String, Object> resultMap = new HashMap<String, Object>(3);
        resultMap.put("code", cn.hutool.http.HttpStatus.HTTP_UNAUTHORIZED);
        resultMap.put("data", e.getMessage());
        resultMap.put("message", "暂未登录或凭证过期");
        String body= JSONUtil.toJsonStr(resultMap);
        DataBuffer buffer =  response.bufferFactory().wrap(body.getBytes(Charset.forName("UTF-8")));
        return response.writeWith(Mono.just(buffer));
    }
}
