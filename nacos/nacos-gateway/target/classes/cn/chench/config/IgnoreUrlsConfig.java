package cn.chench.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 网关白名单配置
 */
@Data
@Component
@ConfigurationProperties(prefix="oauth2.security")
public class IgnoreUrlsConfig {
	
    private List<String> ignoreurls;
    
}
