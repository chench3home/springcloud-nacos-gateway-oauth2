package cn.chench.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import cn.chench.constant.OAuth2Constant;
import reactor.core.publisher.Mono;

/** 
* @author chencaihui 
* @datetime 创建时间：2020年9月29日 下午1:41:15 
*/
@Configuration
public class WsKeyResolver {

	@Bean("tokenKeyResolver")
	public KeyResolver tokenKeyResolver() {
		return exchange -> Mono.just(exchange.getRequest().getHeaders().getFirst(OAuth2Constant.HEADER_AUTHORIZATION));
	}

	@Primary
	@Bean("ipKeyResolver")
	public KeyResolver ipKeyResolver() {
		return exchange -> Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
	}

	@Bean("pathKeyResolver")
	public KeyResolver apiKeyResolver() {
		return exchange -> Mono.just(exchange.getRequest().getPath().value());
	}
	
}
