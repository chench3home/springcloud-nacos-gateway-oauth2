package cn.chench.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.chench.base.BaseResult;


/**
 * @author chencaihui
 * @datetime 创建时间：2020年9月29日 上午9:12:42
 */
@RestController
public class FallbackController {
	
	@GetMapping("/fallback")
	public BaseResult<?> fallback() {
		return BaseResult.error("服务暂时不可用");
	}
	
}
