package cn.chench.exception;

import javax.naming.AuthenticationException;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.chench.base.BaseResult;
import cn.chench.util.LoggerUtil;

/**
 * 全局处理抛出的异常
 */
@ControllerAdvice
public class WsExceptionHandler {
	
    @ResponseBody
    @ExceptionHandler(value = {Exception.class, RuntimeException.class, OAuth2Exception.class, AuthenticationException.class})
    public BaseResult<?> handleOauth2(RuntimeException e) {
    	LoggerUtil.error(e);
        return BaseResult.error(e.getMessage());
    }
}
