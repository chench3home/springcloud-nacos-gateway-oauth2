package cn.chench.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface RoleMapper {
	
	@SelectKey(keyProperty = "id", resultType = String.class, before = true, statement = "select replace(uuid(), '-', '')")
	@Options(keyProperty = "id", useGeneratedKeys = true)
    @Insert("INSERT INTO `oauth_role` (`id`, `name`, `enabled`) VALUES (#{id,jdbcType=VARCHAR}, #{name,jdbcType=VARCHAR}, #{enabled,jdbcType=TINYINT}) ")
    void add(@Param("name") String name, @Param("enabled") Integer enabled);
    
    @Select("SELECT 1 FROM `oauth_role` WHERE name = #{name,jdbcType=VARCHAR}")
    Integer checkName(@Param("name") String name);

    @Update("UPDATE `oauth_role` SET enabled =#{enabled,jdbcType=TINYINT} WHERE name = #{name,jdbcType=VARCHAR} ")
   	void setEnabled(@Param("enabled") Integer enabled, @Param("name") String name);
    
    @Select("SELECT id FROM `oauth_role` WHERE name = #{name,jdbcType=VARCHAR}")
    String getId(@Param("name") String name);
}
