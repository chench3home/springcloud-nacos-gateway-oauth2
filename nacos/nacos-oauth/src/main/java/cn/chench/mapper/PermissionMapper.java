package cn.chench.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

@Mapper
public interface PermissionMapper {
	
	@SelectKey(keyProperty = "id", resultType = String.class, before = true, statement = "select replace(uuid(), '-', '')")
	@Options(keyProperty = "id", useGeneratedKeys = true)
	@Insert("INSERT INTO `oauth_permission` (`id`, `name`, `url`, `method`) VALUES (#{id,jdbcType=VARCHAR}, #{name,jdbcType=VARCHAR}, #{url,jdbcType=VARCHAR}, #{method,jdbcType=VARCHAR}) ")
    void add(@Param("name") String name, @Param("url") String url, @Param("method") String method);
    
    @Select("SELECT 1 FROM `oauth_permission` WHERE url = #{url,jdbcType=VARCHAR}")
    Integer checkUrl(@Param("url") String url);

    @Select("SELECT id FROM `oauth_permission` WHERE url = #{url,jdbcType=VARCHAR}")
    String getId(@Param("url") String url);
}
