package cn.chench.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

@Mapper
public interface RolePermissionMapper {
	
	@SelectKey(keyProperty = "id", resultType = String.class, before = true, statement = "select replace(uuid(), '-', '')")
	@Options(keyProperty = "id", useGeneratedKeys = true)
    @Insert("INSERT INTO `oauth_role_permission` (`id`, `roleid`, `permissionid`) VALUES (#{id,jdbcType=VARCHAR}, #{roleid,jdbcType=VARCHAR}, #{permissionid,jdbcType=VARCHAR}) ")
    void add(@Param("roleid") String roleid, @Param("permissionid") String permissionid);
    
    @Select("SELECT 1 FROM `oauth_role_permission` WHERE roleid = #{roleid,jdbcType=VARCHAR} AND permissionid = #{permissionid,jdbcType=VARCHAR}")
    Integer checkOne(@Param("roleid") String roleid, @Param("permissionid") String permissionid);

 
}
