package cn.chench.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

@Mapper
public interface RoleUserMapper {
	
	@SelectKey(keyProperty = "id", resultType = String.class, before = true, statement = "select replace(uuid(), '-', '')")
	@Options(keyProperty = "id", useGeneratedKeys = true)
    @Insert("INSERT INTO `oauth_role_user` (`id`, `roleid`, `userid`) VALUES (#{id,jdbcType=VARCHAR}, #{roleid,jdbcType=VARCHAR}, #{userid,jdbcType=VARCHAR}) ")
    void add(@Param("roleid") String roleid, @Param("userid") String userid);
    
    @Select("SELECT 1 FROM `oauth_role_user` WHERE roleid = #{roleid,jdbcType=VARCHAR} AND userid = #{userid,jdbcType=VARCHAR}")
    Integer checkOne(@Param("roleid") String roleid, @Param("userid") String userid);

 
}
