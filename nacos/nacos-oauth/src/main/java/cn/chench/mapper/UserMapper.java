package cn.chench.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import cn.chench.model.OAuthUser;

@Mapper
public interface UserMapper {
	
	@SelectKey(keyProperty = "id", resultType = String.class, before = true, statement = "select replace(uuid(), '-', '')")
	@Options(keyProperty = "id", useGeneratedKeys = true)
    @Insert("INSERT INTO `oauth_user` (`id`, `enabled`, `name`, `password`, `locked`, `oauthed`) "
    		+ "VALUES (#{id,jdbcType=VARCHAR}, 1, #{name,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, 0, 1)")
    void add(@Param("name") String name, @Param("password") String password);
	
    @Select("SELECT * FROM oauth_user WHERE name = #{name,jdbcType=VARCHAR} ")
    //@ResultType(OAuthUser.class)
    /*@Results({
    	@Result(property="id", column="id", jdbcType=JdbcType.INTEGER),
    	@Result(property="name", column="name", jdbcType=JdbcType.VARCHAR),
    	@Result(property="password", column="password", jdbcType=JdbcType.VARCHAR),
    	@Result(property="validtime", column="validtime", jdbcType=JdbcType.DATE),
    	@Result(property="locked", column="locked", jdbcType=JdbcType.INTEGER),
    	@Result(property="enabled", column="enabled", jdbcType=JdbcType.INTEGER),
    })*/
    OAuthUser findByUserName(@Param("name") String name);
    
    @Select("SELECT 1 FROM oauth_client_details WHERE client_id = #{clientId,jdbcType=VARCHAR}")
    Integer checkClientId(@Param("clientId") String clientId);
    
    @Select("SELECT 1 FROM oauth_user WHERE name = #{name,jdbcType=VARCHAR}")
    Integer checkName(@Param("name") String name);

    //设置有效时间
    @Update("UPDATE `oauth_user` SET validtime = #{validtime,jdbcType=DATE} WHERE name = #{name,jdbcType=VARCHAR} ")
	void validtime(@Param("validtime") Date validtime, @Param("name") String name);
    
    @Update("UPDATE `oauth_user` SET locked = 1 WHERE name = #{name,jdbcType=VARCHAR} ")
   	void locked(@Param("name") String name);
    
    @Select("select oauth_permission.url from oauth_role_permission \n"
    		+ "INNER JOIN oauth_role on (oauth_role_permission.roleid = oauth_role.id AND oauth_role.enabled = 1) \n"
    		+ "INNER JOIN oauth_permission on oauth_role_permission.permissionid = oauth_permission.id \n"
    		+ "where oauth_role_permission.roleid in (select oauth_role_user.roleid from oauth_role_user where oauth_role_user.userid = #{userid,jdbcType=VARCHAR})" )
    List<String> getPermissions(@Param("userid") String userid);
    

    @Select("select GROUP_CONCAT(oauth_role.`name`) names from oauth_role where oauth_role.enabled =1 "
    		+ "and oauth_role.id in (select oauth_role_user.roleid from oauth_role_user where oauth_role_user.userid = #{userid,jdbcType=VARCHAR})" )
    String getRoleNames(@Param("userid") String userid);
    
    @Select("SELECT id FROM `oauth_user` WHERE name = #{name,jdbcType=VARCHAR}")
    String getId(@Param("name") String name);
}
