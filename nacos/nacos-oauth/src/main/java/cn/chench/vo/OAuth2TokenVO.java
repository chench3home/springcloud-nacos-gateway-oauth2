package cn.chench.vo;

import lombok.Builder;
import lombok.Data;

/**
 * @author chencaihui
 * @datetime 创建时间：2020年10月12日 下午6:24:01
 */
@Data
@Builder
public class OAuth2TokenVO {
	/**
	 * 访问令牌
	 */
	private String token;
	/**
	 * 刷新令牌
	 */
	private String refreshToken;
	/**
	 * 访问令牌头前缀
	 */
	private String tokenHead;
	/**
	 * 有效时间（秒）
	 */
	private int expiresIn;
}
