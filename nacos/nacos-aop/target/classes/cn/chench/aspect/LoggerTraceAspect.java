package cn.chench.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import cn.chench.util.LoggerUtil;

/**
 * 日志traceId设置
 * @author chencaihui
 * @datetime 2020年11月17日 下午6:02:49
 */
@Order(-10)
@Component
@Aspect
public class LoggerTraceAspect {
	
	//around->before->around->after->afterReturning
	@Pointcut("execution(* cn.chench..*.*(..))")
	public void loggerTrace() {}
	
	@Around("loggerTrace()")
    public Object onAround(ProceedingJoinPoint point) throws Throwable {
		LoggerUtil.initTraceId();
    	return point.proceed(point.getArgs());
    }
	
	
}
