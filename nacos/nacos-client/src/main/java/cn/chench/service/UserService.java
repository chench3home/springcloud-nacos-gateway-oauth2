package cn.chench.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.chench.base.BaseResult;
import cn.chench.fallback.UserFallBack;
import cn.chench.model.User;

/** 
* @author chencaihui 
* @datetime 创建时间：2020年7月30日 下午2:20:56 
*/
//服务的生成者（被调用者）在注册中心注册的名字
@FeignClient(name = "nacos-api", fallback = UserFallBack.class)
public interface UserService {

	//@RequestHeader(name=ConstantUtil.HEADER_OAUTH_USER, required=true) String token, 
	//@RequestHeader传递授权请求头
	@PostMapping("/userApi/add")
	public BaseResult<?> add(User user);
	
	@PostMapping("/userApi/modify")
	public BaseResult<?> modify(User user);
	
	@GetMapping("/userApi/getByName")
	public BaseResult<?> getByName(@RequestParam("name") String name);
	
	@GetMapping("/userApi/list")
	public BaseResult<?> list();
	
}
