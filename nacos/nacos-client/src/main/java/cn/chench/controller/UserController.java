package cn.chench.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;

import cn.chench.annotation.OAuth2Token;
import cn.chench.base.BaseController;
import cn.chench.base.BaseResult;
import cn.chench.model.OAuthUser;
import cn.chench.model.User;
import cn.chench.service.UserService;
import cn.chench.util.UserUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author chencaihui
 * @datetime 创建时间：2020年8月4日 上午11:35:46
 */
@DefaultProperties(defaultFallback = "defaultFallback")
@Api("user接口文档")
@RestController
public class UserController extends BaseController{

	@Autowired
	private UserService userService;
	
	@OAuth2Token
	@ApiOperation(value="/user/add", httpMethod="POST", produces="application/json")
	@ResponseBody
	@PostMapping(name="新增用户信息", value="/user/add")
	public BaseResult<?> add(@RequestBody User user){
		OAuthUser oAuthUser =  UserUtil.getOAuthUser();
		user.setCreateid(oAuthUser.getId());
		user.setModifyid(oAuthUser.getId());
		return userService.add(user);
	}
	
	@OAuth2Token
	@ApiOperation(value="/user/modify", httpMethod="POST", consumes="application/json")
	@ResponseBody
	@PostMapping(name="修改用户信息", value="/user/modify")
	public BaseResult<?> modify(@RequestBody User user){
		OAuthUser oAuthUser =  UserUtil.getOAuthUser();
		user.setModifyid(oAuthUser.getId());
		return userService.modify(user);
	}
	
	@OAuth2Token
	@ApiOperation(value="/user/getByName", httpMethod="GET")
	@ApiImplicitParams({
		@ApiImplicitParam(name="name", value="name", dataType="String")
	})
	@ResponseBody
	@GetMapping(name="根据账号获取用户信息", value="/user/getByName")
	public BaseResult<?> getByName(String name){
		return userService.getByName(name);
	}
	
	/*
	单个接口处理
	@HystrixCommand(commandProperties = {
			@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="2000"),//方法执行超时时间，默认值是 1000，即 1秒
	        @HystrixProperty(name="circuitBreaker.enabled", value="true"),
	        @HystrixProperty(name="circuitBreaker.requestVolumeThreshold", value="10"),
	        @HystrixProperty(name="circuitBreaker.sleepWindowInMilliseconds", value="10000"),
	        @HystrixProperty(name="circuitBreaker.errorThresholdPercentage", value="60")
	})*/
	//@OAuth2Token
	@ApiOperation(value="/user/list", httpMethod="GET")
	@ResponseBody
	@GetMapping(name="获取所有用户信息", value="/user/list")
	public BaseResult<?> list(){
		return userService.list();
	}
	
}
