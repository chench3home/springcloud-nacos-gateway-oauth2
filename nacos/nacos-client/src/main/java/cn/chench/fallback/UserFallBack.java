package cn.chench.fallback;

import org.springframework.stereotype.Component;

import cn.chench.base.BaseResult;
import cn.chench.model.User;
import cn.chench.service.UserService;

/** 
* @author chencaihui 
* @datetime 创建时间：2019年4月17日 下午2:19:45 
*/
@Component
public class UserFallBack implements UserService{

	@Override
	public BaseResult<?> add(User user) {
		return BaseResult.error("系统繁忙");
	}

	@Override
	public BaseResult<?> modify(User user) {
		return BaseResult.error("系统繁忙");
	}

	@Override
	public BaseResult<?> getByName(String name) {
		return BaseResult.error("系统繁忙");
	}

	@Override
	public BaseResult<?> list() {
		return BaseResult.error("系统繁忙");
	}
	
}
