package cn.chench.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户信息")
public class User{

	private Long id;
	@ApiModelProperty(name="name", dataType="String")
	private String name;
	@ApiModelProperty(name="phone", dataType="String")
	private String phone;
	@ApiModelProperty(name="email", dataType="String")
	private String email;
	@ApiModelProperty(name="idcard", dataType="String")
	private String idcard;
	@ApiModelProperty(name="sex", dataType="Integer")
	private Integer sex;
	@ApiModelProperty(name="age", dataType="Integer")
	private Integer age;
	@ApiModelProperty(name="job", dataType="String")
	private String job;
	@ApiModelProperty(name="address", dataType="String")
	private String address;
	@ApiModelProperty(name="hjaddress", dataType="String")
	private String hjaddress;
	@ApiModelProperty(name="birthdate", dataType="String")
	private String birthdate;
	@ApiModelProperty(hidden=true)
	private Date createtime;
	@ApiModelProperty(hidden=true)
	private String createid;
	@ApiModelProperty(hidden=true)
	private Date modifytime;
	@ApiModelProperty(hidden=true)
	private String modifyid;
	
}
