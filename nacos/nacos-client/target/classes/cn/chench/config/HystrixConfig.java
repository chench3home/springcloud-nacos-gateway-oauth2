package cn.chench.config;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.hystrix.strategy.HystrixPlugins;

/**
 * @author chencaihui
 * @datetime 创建时间：2020年11月18日 上午11:58:15
 */
@Configuration
public class HystrixConfig {
	
	@PostConstruct
    public void init(){
        HystrixPlugins.getInstance().registerConcurrencyStrategy(
        	feignHystrixConcurrencyStrategy()
        );
    }
	
	@Bean
	public FeignHystrixConcurrencyStrategy feignHystrixConcurrencyStrategy(){
		return new FeignHystrixConcurrencyStrategy();
	}
}
