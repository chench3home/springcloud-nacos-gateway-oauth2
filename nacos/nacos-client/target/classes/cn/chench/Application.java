package cn.chench;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@MapperScan("cn.chench.mapper")
@EnableCircuitBreaker
@SpringBootApplication
@EnableFeignClients
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
    @LoadBalanced//ribbon集群
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
	
	/**
     * 更改 负载均衡策略算法
     * RandomRule #配置规则 随机
     * RoundRobinRule #配置规则 轮询
     * RetryRule #配置规则 重试
     * WeightedResponseTimeRule #配置规则 响应时间权重
     * 也可以自定义负载均衡策略的类(extends AbstractLoadBalancerRule)
     */
    /*@Bean
    public IRule myRule(){
        return new RoundRobinRule();
    }*/
}
