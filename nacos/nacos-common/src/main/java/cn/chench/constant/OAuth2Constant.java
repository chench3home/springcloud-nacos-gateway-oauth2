package cn.chench.constant;

/**
 * 公共常量-oauth2
 * @author chencaihui
 * @datetime 2020年8月5日 下午2:37:51
 */
public interface OAuth2Constant {

    public static final String AUTHORITY_PREFIX = "ROLE_";
    public static final String AUTHORITY_CLAIM_NAME = "authorities";
    
    public static final String HEADER_OAUTH_USER = "oauth2_user";//请求头存储用户信息key
    public static final String HEADER_AUTHORIZATION = "Authorization";//请求头存储用户信息key
    
}
