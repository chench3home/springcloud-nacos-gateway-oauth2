package cn.chench.constant;

/**
 * 公共常量
 * @author chencaihui
 * @datetime 2020年8月5日 下午2:37:51
 */
public interface ConstantUtil {

	public static final int SUCCESS = 0;
	public static final int FAIL = -1;
	public static final int ERROR = 500;

	public static final String UTF8 = "UTF-8";
	public static final int CACHE_TIMEOUT_DEFAULT = 7200;//缓存超时/秒s
	public static final String SPLITE = ":";//分隔符
	
	
	public static final String SESSION_USER = "OAUTH2_CURRENT_USER";//session-key-存放用户对象
	public static final String LOGGER_TRACE_ID = "traceId";//请求头存储用户信息key
}
