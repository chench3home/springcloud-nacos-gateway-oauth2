package cn.chench.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

/**
 * 日志工具
 * @author chencaihui
 * @datetime 创建时间：2019年8月29日 下午2:44:18
 */
public class LoggerUtil {

	private static final ThreadLocal<Long> threadLocal = new ThreadLocal<Long>();
	private static Map<String, Logger> loggerMap = new HashMap<String, Logger>();
	//private static final Logger logger = LoggerFactory.getLogger(LoggerUtil.class);
	private static final Logger logger = LogManager.getLogger(LoggerUtil.class);
	
	private static final Logger getLogger(final String classesName) {
		if (!loggerMap.containsKey(classesName)) {
			try {
				Class<?> classes = Class.forName(classesName);
				final Logger logger = LogManager.getLogger(classes);
				loggerMap.put(classesName, logger);
				ThreadContext.put("currentClassName", classesName);
				return logger;
			} catch (ClassNotFoundException e) {
				logger.error("初始化logger异常", e);
				return logger;
			}
		}
		ThreadContext.put("currentClassName", classesName);
		return loggerMap.get(classesName);
	}

	/**
	 * 初始化traceId
	 * @author chencaihui 
	 * @date 2020年11月18日 上午8:39:14
	 */
	public static void initTraceId(){
		String traceId = null;
		HttpServletRequest request = RequestUtil.getRequest();
		if(request!=null){
			traceId = request.getHeader("traceId");
		}
		if(NullUtil.isNull(traceId)){
			traceId = ThreadContext.get("traceId");
		}
		if(NullUtil.isNull(traceId)){
			traceId = IdHelper.getUUID();
		}
		if(NullUtil.isNotNull(traceId)){
			ThreadContext.put("traceId", traceId);
		}
	}
	
	/**
	 * 记录开始时间戳
	 * @author chencaihui
	 * @date 2019年12月13日 上午11:22:48
	 */
	public static void start() {
		long time = System.currentTimeMillis();
		threadLocal.set(time);
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		getLogger(stackTraceElement.getClassName()).info(
				stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "--->开始start..." + time);
	}
	
	/**
	 * 记录开始时间戳
	 * @author chencaihui
	 * @date 2019年12月13日 上午11:22:48
	 */
	public static void start(Object... msg) {
		long time = System.currentTimeMillis();
		threadLocal.set(time);
		StringBuilder msgsb = new StringBuilder();
		for (Object message : msg) {
			msgsb.append("--->" + String.valueOf(message));
		}
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		getLogger(stackTraceElement.getClassName()).info(
				stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + msgsb.toString() + "--->开始start...");
	}
	
	/**
	 * 计算耗时
	 * @author chencaihui
	 * @date 2019年12月13日 上午11:23:04
	 */
	public static void end() {
		StringBuilder msgsb = new StringBuilder();
		Long startTime = threadLocal.get();
		if (startTime != null && startTime.longValue() > 0) {
			msgsb.append("--->耗时(毫秒)：" + (System.currentTimeMillis() - startTime.longValue()));
			threadLocal.remove();// 置空
		}
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		getLogger(stackTraceElement.getClassName())
				.info(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + msgsb.toString());
	}

	/**
	 * 计算耗时
	 * @author chencaihui
	 * @date 2019年12月13日 上午11:23:04
	 * @param msg
	 */
	public static void end(Object... msg) {
		StringBuilder msgsb = new StringBuilder();
		for (Object message : msg) {
			msgsb.append("--->" + String.valueOf(message));
		}
		Long startTime = threadLocal.get();
		if (startTime != null && startTime.longValue() > 0) {
			msgsb.append("--->耗时(毫秒)：" + (System.currentTimeMillis() - startTime.longValue()));
			threadLocal.remove();// 置空
		}
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		getLogger(stackTraceElement.getClassName())
				.info(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + msgsb.toString());
	}

	public static void info(Object... msg) {
		StringBuilder msgsb = new StringBuilder();
		for (Object message : msg) {
			msgsb.append("--->" + String.valueOf(message));
		}
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		getLogger(stackTraceElement.getClassName())
				.info(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + msgsb.toString());
	}

	public static void warn(Object... msg) {
		StringBuilder msgsb = new StringBuilder();
		for (Object message : msg) {
			msgsb.append("--->" + String.valueOf(message));
		}
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		getLogger(stackTraceElement.getClassName())
				.warn(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + msgsb.toString());
	}

	public static void error(Object... msg) {
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		String className = stackTraceElement.getClassName();
		String methodName = stackTraceElement.getMethodName();
		StringBuilder msgsb = new StringBuilder();
		for (Object message : msg) {
			msgsb.append("--->" + String.valueOf(message));
		}
		getLogger(className).error(className + "." + methodName + "--->" + msgsb.toString());
	}

	public static void error(Exception e) {
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		String className = stackTraceElement.getClassName();
		String methodName = stackTraceElement.getMethodName();
		String errormsg = getErrorMessage(e);
		getLogger(className).error(className + "." + methodName + "--->" + errormsg);
	}

	public static void error(String msg, Exception e) {
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		String className = stackTraceElement.getClassName();
		String methodName = stackTraceElement.getMethodName();
		String errormsg = getErrorMessage(e);
		getLogger(className).error(className + "." + methodName + "--->" + msg + "：" + errormsg);
	}
	
	public static void error(String msg, Throwable e) {
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
		String className = stackTraceElement.getClassName();
		String methodName = stackTraceElement.getMethodName();
		String errormsg = getErrorMessage(e);
		getLogger(className).error(className + "." + methodName + "--->" + msg + "：" + errormsg);
	}

	private static final String getErrorMessage(final Exception ex) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(baos);){
			ex.printStackTrace(ps);
			return baos.toString();
		} catch (Exception e) {
			return ex.getMessage();
		}
	}
	
	private static final String getErrorMessage(final Throwable ex) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(baos);){
			ex.printStackTrace(ps);
			return baos.toString();
		} catch (Exception e) {
			return ex.getMessage();
		}
	}
	
}
