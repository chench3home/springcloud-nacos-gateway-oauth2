package cn.chench.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OAuthPermission {

	private String id;
	private String name;
	private String url;
	private String method;
}
