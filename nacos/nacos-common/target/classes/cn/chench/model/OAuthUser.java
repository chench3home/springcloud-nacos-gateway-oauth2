package cn.chench.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OAuthUser {

	private String id;
	private String name;
	private String password;
	private Date validtime;
	private Integer locked;
	private Integer enabled;
}
