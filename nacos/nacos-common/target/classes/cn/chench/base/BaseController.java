package cn.chench.base;

/**
 * 公共熔断返回
 * @author chencaihui
 * @datetime 创建时间：2020年10月29日 上午8:37:46
 */
public class BaseController {

	// 默认熔断
	protected BaseResult<?> defaultFallback() {
		return BaseResult.error("系统繁忙");
	}
}
