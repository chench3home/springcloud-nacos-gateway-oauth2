package cn.chench.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import cn.chench.constant.ConstantUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 响应
 * @author chencaihui
 * @datetime 创建时间：2020年8月5日 下午2:40:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("unchecked")
public class BaseResult<T> implements Serializable {

	private static final long serialVersionUID = -2410539351914134704L;
	private int code;
	private String message;
	private T data;
	private T[] datas;

	public static <T> BaseResult<T> success() {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.SUCCESS);
		baseResult.setMessage("操作成功");
		return baseResult;
	}

	public static <T> BaseResult<T> success(String message) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.SUCCESS);
		baseResult.setMessage(message);
		return baseResult;
	}

	public static <T> BaseResult<T> success(String message, T data) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.SUCCESS);
		baseResult.setMessage(message);
		baseResult.setData(data);
		return baseResult;
	}

	public static <T> BaseResult<T> success(String message, T... datas) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.SUCCESS);
		baseResult.setMessage(message);
		baseResult.setDatas(datas);
		return baseResult;
	}

	public static <T> BaseResult<T> success(T data) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.SUCCESS);
		baseResult.setMessage("操作成功");
		baseResult.setData(data);
		return baseResult;
	}

	public static <T> BaseResult<T> success(T... datas) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.SUCCESS);
		baseResult.setMessage("操作成功");
		baseResult.setDatas(datas);
		return baseResult;
	}

	public static <T> BaseResult<T> fail() {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.FAIL);
		baseResult.setMessage("操作失败");
		return baseResult;
	}

	public static <T> BaseResult<T> fail(String message) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.FAIL);
		baseResult.setMessage(message);
		return baseResult;
	}

	public static <T> BaseResult<T> fail(String message, Throwable e) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.FAIL);
		baseResult.setMessage(e.getMessage());
		return baseResult;
	}

	public static <T> BaseResult<T> fail(String message, Exception e) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.FAIL);
		baseResult.setMessage(e.getMessage());
		return baseResult;
	}

	public static <T> BaseResult<T> error() {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.ERROR);
		baseResult.setMessage("发生错误");
		return baseResult;
	}

	public static <T> BaseResult<T> error(String message) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.ERROR);
		baseResult.setMessage(message);
		return baseResult;
	}

	public static <T> BaseResult<T> error(Throwable e) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.ERROR);
		baseResult.setMessage(e.getMessage());
		return baseResult;
	}

	public static <T> BaseResult<T> error(Exception e) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(ConstantUtil.ERROR);
		baseResult.setMessage(e.getMessage());
		return baseResult;
	}

	public static <T> BaseResult<T> result(int code) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(code);
		return baseResult;
	}

	public static <T> BaseResult<T> result(int code, String message) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(code);
		baseResult.setMessage(message);
		return baseResult;
	}

	public static <T> BaseResult<T> result(int code, T data) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(code);
		baseResult.setData(data);
		return baseResult;
	}

	public static <T> BaseResult<T> result(int code, T... datas) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(code);
		baseResult.setDatas(datas);
		return baseResult;
	}

	public static <T> BaseResult<T> result(int code, String message, T data) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(code);
		baseResult.setMessage(message);
		baseResult.setData(data);
		return baseResult;
	}

	public static <T> BaseResult<T> result(int code, String message, T... datas) {
		BaseResult<T> baseResult = new BaseResult<T>();
		baseResult.setCode(code);
		baseResult.setMessage(message);
		baseResult.setDatas(datas);
		return baseResult;
	}
	
	public static <T> Map<String, Object> successMap(String message) {
		Map<String, Object> resultMap = new HashMap<String, Object>(2);
		resultMap.put("code", ConstantUtil.SUCCESS);
		resultMap.put("message", message);
		return resultMap;
	}
	
	public static <T> Map<String, Object> successMap(T data) {
		Map<String, Object> resultMap = new HashMap<String, Object>(2);
		resultMap.put("code", ConstantUtil.SUCCESS);
		resultMap.put("data", data);
		return resultMap;
	}
	
	public static <T> Map<String, Object> failMap(String message) {
		Map<String, Object> resultMap = new HashMap<String, Object>(2);
		resultMap.put("code", ConstantUtil.FAIL);
		resultMap.put("message", message);
		return resultMap;
	}
	
	public static <T> Map<String, Object> errorMap(String message) {
		Map<String, Object> resultMap = new HashMap<String, Object>(2);
		resultMap.put("code", ConstantUtil.ERROR);
		resultMap.put("message", message);
		return resultMap;
	}
	
	public static <T> Map<String, Object> resultMap(int code) {
		Map<String, Object> resultMap = new HashMap<String, Object>(1);
		resultMap.put("code", code);
		return resultMap;
	}
	
	public static <T> Map<String, Object> resultMap(int code, String message) {
		Map<String, Object> resultMap = new HashMap<String, Object>(2);
		resultMap.put("code", code);
		resultMap.put("message", message);
		return resultMap;
	}
	
	public static <T> Map<String, Object> resultMap(int code, T data) {
		Map<String, Object> resultMap = new HashMap<String, Object>(2);
		resultMap.put("code", code);
		resultMap.put("data", data);
		return resultMap;
	}
	
	public static <T> Map<String, Object> resultMap(int code, T... datas) {
		Map<String, Object> resultMap = new HashMap<String, Object>(2);
		resultMap.put("code", code);
		resultMap.put("datas", datas);
		return resultMap;
	}
	
	public static <T> Map<String, Object> resultMap(int code, String message, T data) {
		Map<String, Object> resultMap = new HashMap<String, Object>(3);
		resultMap.put("code", code);
		resultMap.put("message", message);
		resultMap.put("data", data);
		return resultMap;
	}
	
	public static <T> Map<String, Object> resultMap(int code, String message, T... datas) {
		Map<String, Object> resultMap = new HashMap<String, Object>(3);
		resultMap.put("code", code);
		resultMap.put("message", message);
		resultMap.put("datas", datas);
		return resultMap;
	}

}
