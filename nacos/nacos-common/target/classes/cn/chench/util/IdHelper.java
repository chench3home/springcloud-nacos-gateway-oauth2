package cn.chench.util;

import java.util.UUID;

/** 
* id工具类
* @author chencaihui 
* @datetime 创建时间：2017年3月27日 上午10:03:30 
*/
public class IdHelper{

	/**
	 * 获取uuid
	 *@author chencaihui 
	 */
	public static final String getUUID(){
		try {
			return UUID.randomUUID().toString().replace("-", "");
		} catch (Exception e) {
			return RandomUtil.getRandomString(32);
		}
	}
	
}
