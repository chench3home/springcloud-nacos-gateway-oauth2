package cn.chench.util;

import cn.chench.constant.ConstantUtil;
import cn.chench.model.OAuthUser;

/** 
* @author chencaihui 
* @datetime 创建时间：2020年10月26日 上午11:16:19 
*/
public class UserUtil {

	/**
	 * 获取授权用户
	 * @author chencaihui 
	 * @date 2020年10月26日 上午11:17:18 
	 */
	public static OAuthUser getOAuthUser(){
		try {
			return (OAuthUser) RequestUtil.getRequest().getSession().getAttribute(ConstantUtil.SESSION_USER);
		} catch (Exception e) {
			LoggerUtil.error(e);
			return null;
		}
	}
	
	/**
	 * 获取id
	 * @author chencaihui 
	 * @date 2020年10月26日 上午11:20:18 
	 */
	public static String getUserId(){
		OAuthUser user = getOAuthUser();
		return user==null?null:user.getId();
	}
	
}
