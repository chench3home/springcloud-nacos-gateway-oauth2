package cn.chench.constant;

/**
 * 公共常量-redis
 * @author chencaihui
 * @datetime 2020年10月14日 上午10:39:54
 */
public class RedisConstant {

    public static final String OAUTH2_USER_PERMISSIONS = "oauth2:user_permission:";//用户登录角色
    public static final String OAUTH2_LOCKED = "oauth2:locked:";
    public static final String OAUTH2_DISABLED = "oauth2:disabled:";
    public static final String OAUTH2_EXPIRED = "oauth2:expired:";
    public static final String OAUTH2_LOGINFAIL = "oauth2:loginfail:";
	
}
