package cn.chench.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

	private String id;
	private String name;
	private String phone;
	private String email;
	private String idcard;
	private Integer sex;
	private Integer age;
	private String job;
	private String address;
	private String hjaddress;
	private String birthdate;
	private Date createtime;
	private String createid;
	private Date modifytime;
	private String modifyid;

}
