package cn.chench.service;

import java.util.List;

import cn.chench.model.User;

/** 
* @author chencaihui 
* @datetime 创建时间：2020年10月26日 上午9:41:43 
*/
public interface IUserService {
	
	User getByName(String name);
	
	List<User> getList();
    
    void add(User user);
    
    void modify(User user);
    
}
