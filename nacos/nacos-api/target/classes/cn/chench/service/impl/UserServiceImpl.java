package cn.chench.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.chench.mapper.UserMapper;
import cn.chench.model.User;
import cn.chench.service.IUserService;

/** 
* @author chencaihui 
* @datetime 创建时间：2020年10月26日 上午9:43:08 
*/
@Service
public class UserServiceImpl implements IUserService {

	@Autowired
    private UserMapper userMapper;

	@Transactional(readOnly=true)
	@Override
	public User getByName(String name) {
		return userMapper.getByName(name);
	}
	
	@Transactional(readOnly=true)
	@Override
	public List<User> getList() {
		return userMapper.getList();
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void add(User user) {
		userMapper.add(user);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void modify(User user) {
		userMapper.modify(user);
	}
}
