package cn.chench.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.chench.annotation.OAuth2Token;
import cn.chench.base.BaseResult;
import cn.chench.model.User;
import cn.chench.service.IUserService;

/**
 * @author chencaihui
 * @datetime 创建时间：2020年8月4日 上午11:35:46
 */
@RestController
@RefreshScope
public class UserController {

	@Autowired
	private IUserService userService;
	@Value("${chench}")
    private String chench;
	
	@OAuth2Token
	//http://localhost:6631/actuator/bus-refresh
	//手动刷新客户端配置
	@PostMapping("/userApi/add")
	public BaseResult<?> add(@RequestBody User user){
		userService.add(user);
		return BaseResult.success();
	}
	
	@OAuth2Token
	@PostMapping("/userApi/modify")
	public BaseResult<?> modify(@RequestBody User user){
		userService.modify(user);
		return BaseResult.success();
	}
	
	@OAuth2Token
	@GetMapping("/userApi/getByName")
	public BaseResult<?> getByName(@RequestParam("name") String name){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		User user = userService.getByName(name);
		resultMap.put("datas", user);
		resultMap.put("chench", chench);
		resultMap.put("time", System.currentTimeMillis());
		return BaseResult.success(resultMap);
	}
	
	//@OAuth2Token
	@GetMapping("/userApi/list")
	public BaseResult<?> list(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<User> users = userService.getList();
		resultMap.put("datas", users);
		resultMap.put("chench", chench);
		resultMap.put("time", System.currentTimeMillis());
		return BaseResult.success(resultMap);
	}
}
