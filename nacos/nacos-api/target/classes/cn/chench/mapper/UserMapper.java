package cn.chench.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import cn.chench.model.User;

@Mapper
public interface UserMapper {
	
    @Select("SELECT * FROM ch_user WHERE name = #{name,jdbcType=VARCHAR} ")
    User getByName(String name);
    
    @Select("SELECT * FROM ch_user ")
    List<User> getList();
    
    @SelectKey(keyProperty = "id", resultType = String.class, before = true, statement = "select replace(uuid(), '-', '')")
	@Options(keyProperty = "id", useGeneratedKeys = true)
    @Insert("INSERT INTO `ch_user` (`id`, `name`, `phone`, `email`, `idcard`, `sex`, `age`, "
    		+ "`job`, `address`, `hjaddress`, `birthdate`, `createid`, `modifyid`) "
    		+ "VALUES (#{id,jdbcType=VARCHAR}, #{name,jdbcType=VARCHAR}, #{phone,jdbcType=VARCHAR}, #{email,jdbcType=VARCHAR}, #{idcard,jdbcType=VARCHAR}, #{sex,jdbcType=TINYINT}, #{age,jdbcType=SMALLINT}, "
    		+ "#{job,jdbcType=VARCHAR}, #{address,jdbcType=VARCHAR}, #{hjaddress,jdbcType=VARCHAR}, #{birthdate,jdbcType=DATE}"
    		+ ", #{createid,jdbcType=VARCHAR}, #{modifyid,jdbcType=VARCHAR})")
    void add(User user);
    
    @Update("UPDATE ch_user SET modifytime = CURRENT_TIMESTAMP, modifyid = #{modifyid,jdbcType=BIGINT} "
    		+ " WHERE name = #{name,jdbcType=VARCHAR} ")
    void modify(User user);
}
