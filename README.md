# springcloud-nacos-gateway-oauth2

#### 介绍

- 将eureka替换为nacos
- nacos-gateway  路由
- nacos-oauth 统一鉴权
- nacos-client feign熔断降级+ribbon负载均衡
- nacos-api 业务接口


#### 软件架构
- nacos-gateway
- nacos-oauth
- nacos-client
- nacos-api


#### 安装教程

1. 安装nacos注册中心
2. 启动nacos服务
3. git clone https://gitee.com/chench3home/springcloud-nacos-gateway-oauth2.git
4. 下载源码到本地并修改mysql、redis相关配置
5. 运行

#### 使用说明

1. 参考代码，并结合学习知识。
2. <spring-cloud.version>Hoxton.SR5</spring-cloud.version>
3. <spring-boot.version>2.3.0.RELEASE</spring-boot.version>
4. <nacos.version>0.2.2.RELEASE</nacos.version>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
